using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawn : MonoBehaviour
{
    public int count;
    public GameObject obj;
    public GameObject tmp;
    public int i = 0;
    public Vector2 spawnpoint;
    float x = 0.0f;
    float y = 0.0f;
    public float radius;
    public float timeCountDown = 0;
    public float tmpCount = 0;
    public float diameter;
    public float offset;
    public float resize = 0.7f;
    public void Start()
    {

        offset = transform.localScale.x <= transform.localScale.y ? transform.localScale.x / transform.localScale.y : transform.localScale.y / transform.localScale.x;
        if (offset >= 0.86)
        {
            diameter = transform.localScale.x < transform.localScale.y ? (transform.localScale.x / (offset * count * 0.1f)) * 0.7f : (transform.localScale.y / (offset * count * 0.1f)) * 0.7f;
        }
        if (offset < 0.86 && offset >= 0.5)
        {
            diameter = transform.localScale.x < transform.localScale.y ? ((transform.localScale.x / (offset * count * offset) + transform.localScale.y / (offset * count * (1 - offset)))) * resize : ((transform.localScale.x / (offset * count * (1 - offset)) + transform.localScale.y / (offset * count * offset))) * resize;
        }
        if (offset < 0.5)
        {
            diameter = transform.localScale.x < transform.localScale.y ? ((transform.localScale.x / (count * 0.1f))) : ((transform.localScale.y / (count * 0.1f)));
        }
        obj.transform.localScale = new Vector3(diameter, diameter, diameter);
        while (i < 100)
        {
            if (obj.transform.localScale.x < 0.5f * diameter && obj.transform.localScale.y < 0.5f * diameter && obj.transform.localScale.z < 0.5f * diameter)
            {
                obj.transform.localScale = new Vector3(0.5f * diameter, 0.5f * diameter, 0.5f * diameter);
            }
            radius = obj.GetComponent<CircleCollider2D>().bounds.extents.y;
            x = Random.Range(-transform.localScale.x / 2 + obj.transform.localScale.x / 2, transform.localScale.x / 2 - obj.transform.localScale.x / 2);
            y = Random.Range(-transform.localScale.y / 2 + obj.transform.localScale.y / 2, transform.localScale.y / 2 - obj.transform.localScale.y / 2);
            spawnpoint = new Vector2(x, y);

            tmpCount++;

            Collider2D collider = Physics2D.OverlapCircle(spawnpoint, radius, LayerMask.GetMask("Circle"));
            if (collider == false)
            {
                i++;
                tmp = (GameObject)Instantiate(obj, spawnpoint, Quaternion.identity);
                tmp.name = i.ToString();
            }
            else
            {
                timeCountDown++;

            }
            if (timeCountDown >= 1000 && obj.transform.localScale.x > 0.01f * diameter && obj.transform.localScale.y > 0.01f * diameter && obj.transform.localScale.z > 0.01f * diameter)
            {
                float X = obj.transform.localScale.x;
                float Y = obj.transform.localScale.y;
                float Z = obj.transform.localScale.z;
                X -= 0.01f * diameter;
                Y -= 0.01f * diameter;
                Z -= 0.01f * diameter;
                obj.transform.localScale = new Vector3(X, Y, Z);
                timeCountDown = 0;
            }
            if (tmpCount >= 100000)
            {
                break;
            }
        }
    }
}
